"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var body_parser_1 = __importDefault(require("body-parser"));
var cors_1 = __importDefault(require("cors"));
var PrimeFactor_1 = __importDefault(require("./PrimeFactor"));
var PORT = 3000;
var app = (0, express_1.default)();
app.use(body_parser_1.default.json());
app.use((0, cors_1.default)());
app.use(express_1.default.static("public"));
var task;
var factors = [];
var logs = [];
app.get("/", express_1.default.static("public"));
app.get("/task", function (_, res) {
    console.log("GET Request:\n- Endpoint: /task\n");
    if (!task) {
        res.status(400);
        res.send("No task waiting");
        return;
    }
    res.send(task);
});
app.post("/task", function (req, res) {
    console.log("POST Request:\n- Endpoint: /task\n- Request: ".concat(JSON.stringify(req.body), "\n"));
    var newTask = req.body;
    if (!newTask.number || newTask.number < 0) {
        res.status(400);
        res.send("Error: Failed to parse task");
        return;
    }
    task = newTask;
    logs = [];
    factors = [];
    res.status(200);
    res.send("Success");
    return;
});
app.get("/factors", function (_, res) {
    console.log("GET Request:\n- Endpoint: /factors\n");
    res.send(factors);
});
app.get("/logs", function (_, res) {
    console.log("GET Request:\n- Endpoint: /logs\n");
    res.send(logs);
});
app.post("/result", function (req, res) {
    console.log("POST Request:\n- Endpoint: /result\n- Request: ".concat(JSON.stringify(req.body), "\n"));
    var sendError = function (message) {
        res.status(400);
        res.send("Error: ".concat(message));
        return;
    };
    if (!task) {
        sendError("No task exists");
        return;
    }
    var newResult = req.body;
    if (!newResult || !newResult.number) {
        sendError("Failed to parse result");
        return;
    }
    if (newResult.number != task.number) {
        sendError("Result does not match current task of ".concat(task.number));
        return;
    }
    addResult(newResult);
    res.status(200);
    res.send("Success");
    return;
});
var addResult = function (result) {
    console.log("ADDING RESULT");
    logs.push(result.log);
    factors.push(new PrimeFactor_1.default(result.prime, result.result, result.language));
};
app.listen(PORT, function () {
    console.log("Listening...");
});
//# sourceMappingURL=index.js.map