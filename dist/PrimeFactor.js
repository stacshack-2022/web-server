"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PrimeFactor = (function () {
    function PrimeFactor(prime, count, language) {
        this.prime = prime;
        this.count = count;
        this.language = language;
    }
    return PrimeFactor;
}());
exports.default = PrimeFactor;
//# sourceMappingURL=PrimeFactor.js.map