"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("uuid");
var Task = (function () {
    function Task(number) {
        this.id = (0, uuid_1.v4)();
        this.number = number;
    }
    return Task;
}());
exports.default = Task;
//# sourceMappingURL=Task.js.map