"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Result = (function () {
    function Result(number, prime, result, language, log) {
        this.number = number;
        this.prime = prime;
        this.result = result;
        this.language = language;
        this.log = log;
    }
    return Result;
}());
exports.default = Result;
//# sourceMappingURL=Result.js.map