import express, { Request, Response } from "express";
import bodyParser from "body-parser";
import Task from "./Task";
import Result from "./Result";
import cors from "cors";
import PrimeFactor from "./PrimeFactor";

const PORT = 3000;

const app = express();
app.use(bodyParser.json());
app.use(cors());
app.use(express.static("public"));

let task: Task;
let factors: PrimeFactor[] = [];
let logs: string[] = [];

app.get("/", express.static("public"));

app.get("/task", (_, res: Response) => {
  console.log("GET Request:\n- Endpoint: /task\n");

  if (!task) {
    res.status(400);
    res.send("No task waiting");
    return;
  }
  res.send(task);
});

app.post("/task", (req: Request, res: Response) => {
  console.log(
    `POST Request:\n- Endpoint: /task\n- Request: ${JSON.stringify(req.body)}\n`
  );

  const newTask: Task = req.body as Task;

  if (!newTask.number || newTask.number < 0) {
    res.status(400);
    res.send("Error: Failed to parse task");
    return;
  }

  task = newTask;
  logs = [];
  factors = [];
  res.status(200);
  res.send("Success");
  return;
});

app.get("/factors", (_, res: Response) => {
  console.log("GET Request:\n- Endpoint: /factors\n");
  res.send(factors);
});

app.get("/logs", (_, res: Response) => {
  console.log("GET Request:\n- Endpoint: /logs\n");
  res.send(logs);
});

app.post("/result", (req: Request, res: Response) => {
  console.log(
    `POST Request:\n- Endpoint: /result\n- Request: ${JSON.stringify(
      req.body
    )}\n`
  );

  const sendError = (message: string) => {
    res.status(400);
    res.send(`Error: ${message}`);
    return;
  };

  if (!task) {
    sendError("No task exists");
    return;
  }

  const newResult: Result = req.body as Result;

  if (!newResult || !newResult.number) {
    sendError("Failed to parse result");
    return;
  }

  if (newResult.number != task.number) {
    sendError(`Result does not match current task of ${task.number}`);
    return;
  }

  addResult(newResult);
  res.status(200);
  res.send("Success");
  return;
});

const addResult = (result: Result) => {
  console.log("ADDING RESULT");
  logs.push(result.log);
  factors.push(new PrimeFactor(result.prime, result.result, result.language));
};

app.listen(PORT, () => {
  console.log("Listening...");
});
