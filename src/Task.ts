import { v4 as uuidv4 } from "uuid";

export default class Task {
  id: string;
  number: number;

  constructor(number: number) {
    this.id = uuidv4();
    this.number = number;
  }
}
