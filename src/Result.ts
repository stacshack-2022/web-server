export default class Result {
  number: number;

  prime: number;
  result: number;

  language: string;
  log: string;

  constructor(
    number: number,
    prime: number,
    result: number,
    language: string,
    log: string
  ) {
    this.number = number;
    this.prime = prime;
    this.result = result;
    this.language = language;
    this.log = log;
  }
}
