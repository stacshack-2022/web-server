export default class PrimeFactor {
  prime: number;
  count: number;
  language: string;

  constructor(prime: number, count: number, language: string) {
    this.prime = prime;
    this.count = count;
    this.language = language;
  }
}
